<?php /* Template Name: Collections Page */ ?>

<?php get_header(); ?>

<div class="main-wrap" id="app">

    <div class="main-wrap-top-bar">
        <div class="container">
            <div class="search-filter">
                <i class="fa fa-search"></i>
                <input type="text" class="search-field" placeholder="Search Collections" v-model="search_filter">
            </div>
            <div class="folders-navigation">
                <li :class="isActiveFolder('all')" v-on:click="current_active_folder = 'all'">
                    <?php _e('All','kurayami'); ?>
                </li>
                <li v-for="folder in folders" :class="[isActiveFolder(folder.name), isParentFolder(folder)]">
                    <div v-if="folder.folders == 0" v-on:click="current_active_folder = folder.name">
                        {{folder.name}}
                    </div>
                    <div v-if="folder.folders != 0">
                        {{folder.name}} <i class="ionicons ion-arrow-down-b"></i>
                        <ul class="subfolders-container">

                            <submenu-item v-for="(subfolder, index) in folder.children" :key="index" :item="subfolder" :current-active-folder="current_active_folder"></submenu-item>

                        </ul>
                    </div>
                </li>
            </div>
            <div class="order-by-filter">
                <span class="fixed-color"><?php _e('Order By','kurayami'); ?></span> {{ordering_by}} <i class="ionicons ion-arrow-down-b"></i>
                <div class="order-by-select">
                    <li v-on:click="changeOrderingBy('Name')" v-if="ordering_by != 'Name'">
                        <?php _e('Name','kurayami'); ?>
                    </li>
                    <li v-on:click="changeOrderingBy('Publishing Date')" v-if="ordering_by != 'Publishing Date'">
                        <?php _e('Publishing Date','kurayami'); ?>
                    </li>
                    <li v-on:click="changeOrderingBy('# of Photos')" v-if="ordering_by != '# of Photos'">
                        <?php _e('# of Photos','kurayami'); ?>
                    </li>
                </div>
            </div>
        </div>
    </div>

    <div class="collections-grid container">
        <div class="collection-card-container" v-for="collection in ordered_collections">
            <a :href="collection.permalink" class="collection-card">
                <div class="featured-image" :data-bg="collection.featured_image"></div>
                <div class="collection-metadatas">
                    <h3>{{collection.name}}</h3>
                    <div class="photos-count">{{collection.photos}} Photos</div>
                </div>
            </a>
        </div>
    </div>

</div>

<?php 
global $photocore;
$collections = $photocore->get_all_collections();
$folders_hierarchy = $photocore->get_folder_hierarchy();

// Cleaning Collections
foreach ($collections as &$collection) {
    $collection['featured_image'] = get_the_post_thumbnail_url($collection['id'],'large');
    $collection['permalink'] = get_permalink($collection['id']);
    $terms = get_the_terms( $collection['id'], 'meow_folder' );
    if($terms) {
        $collection['folder'] = $terms[0]->name;
    } else {
        $collection['folder'] = 'null';
    }
}

$php_datas = array(
    'folders' => $folders_hierarchy,
    'collections' => $collections
);

wp_localize_script( 'collections-pages-js', 'php_data', $php_datas );

get_footer(); ?>