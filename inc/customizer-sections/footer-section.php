<?php
$wp_customize->add_section( 'footer_section', array(
    'priority' => 10,
    'capability' => 'edit_theme_options',
    'theme_supports' => '',
    'title' => __( 'Footer', 'kurayami' ),
    'description' => '',
    'panel' => 'theme_settings',
) );

    /*  =============================================== */
    //  ## COPYRIGHT
    /*  =============================================== */
    $wp_customize->add_control(
        new kurayami_Customizer_Accordion(
            $wp_customize,
            'kurayami-copyright-accordion',
            array(
                'section' => 'footer_section',
                'label' => __( 'Footer', 'kurayami' ),
                'type' => 'accordion'
            )
        )
    );

    $wp_customize->add_setting(
        'footer_copyright_text',
        array(
            'default' => 'Kurayami Theme by <a href="http://meowapps.com">MeowApps</a>',
            'transport' => 'refresh',
            'sanitize_callback' => "wp_kses_post",
        )
    );
    $wp_customize->add_control(
        'footer_copyright_text',
        array(
            'label' => __('Copyright Text', 'kurayami'),
            'description' => __('This text will be displayed at the bottom of every pages of your website, usually a copyright informations.', 'kurayami'),
            'section' => 'footer_section',
            'type' => 'text'
        )
    );

    $wp_customize->add_setting( 'display_sns_in_footer', array(
        'default' => true,
        'transport' => 'refresh',
    ) );
    $wp_customize->add_control(
        'display_sns_in_footer',
        array(
            'type' => 'checkbox',
            'label' => __('Display Social Networks', 'kurayami'),
            'section' => 'footer_section',
        )
    );


