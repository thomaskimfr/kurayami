<?php

/*  =============================================== */
//  # GENERAL SECTION
/*  =============================================== */
$wp_customize->add_section( 'general_section', array(
    'priority' => 10,
    'capability' => 'edit_theme_options',
    'theme_supports' => '',
    'title' => __( 'General', 'kurayami' ),
    'description' => '',
    'panel' => 'theme_settings',
) );

    /*  =============================================== */
    //  ## SOCIAL NETWORKS
    /*  =============================================== */
    $wp_customize->add_control(
        new kurayami_Customizer_Accordion(
            $wp_customize,
            'kurayami-social-networks-accordion',
            array(
                'section' => 'general_section',
                'label' => __( 'Social Networks', 'kurayami' ),
                'type' => 'accordion'
            )
        )
    );   

    $wp_customize->add_setting(
        'instagram_url',
        array(
            'default' => '',
            'transport' => 'refresh',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control(
        'instagram_url',
        array(
            'label' => __('Instagram URL', 'kurayami'),
            'description' => __('To hide it, leave it blank.', 'kurayami'),
            'section' => 'general_section',
            'type' => 'text'
        )
    );

    $wp_customize->add_setting(
        'facebook_url',
        array(
            'default' => '',
            'transport' => 'refresh',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control(
        'facebook_url',
        array(
            'label' => __('Facebook URL', 'kurayami'),
            'description' => __('To hide it, leave it blank.', 'kurayami'),
            'section' => 'general_section',
            'type' => 'text'
        )
    );

    $wp_customize->add_setting(
        'twitter_url',
        array(
            'default' => '',
            'transport' => 'refresh',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control(
        'twitter_url',
        array(
            'label' => __('Twitter URL', 'kurayami'),
            'description' => __('To hide it, leave it blank.', 'kurayami'),
            'section' => 'general_section',
            'type' => 'text'
        )
    );

    $wp_customize->add_setting(
        'cinqcentpx_url',
        array(
            'default' => '',
            'transport' => 'refresh',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control(
        'cinqcentpx_url',
        array(
            'label' => __('500Px URL', 'kurayami'),
            'description' => __('To hide it, leave it blank.', 'kurayami'),
            'section' => 'general_section',
            'type' => 'text'
        )
    );

    $wp_customize->add_setting(
        'flickr_url',
        array(
            'default' => '',
            'transport' => 'refresh',
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control(
        'flickr_url',
        array(
            'label' => __('Flickr URL', 'kurayami'),
            'description' => __('To hide it, leave it blank.', 'kurayami'),
            'section' => 'general_section',
            'type' => 'text'
        )
    );

    /*  =============================================== */
    //  ## OPTIMIZATION
    /*  =============================================== */
    $wp_customize->add_control(
        new kurayami_Customizer_Accordion(
            $wp_customize,
            'kurayami-optimization-accordion',
            array(
                'section' => 'general_section',
                'label' => __( 'Optimization', 'kurayami' ),
                'type' => 'accordion'
            )
        )
    );

    $wp_customize->add_setting( 'disable_parralax_post_hero', array(
        'default' => false,
        'transport' => 'refresh',
    ) );
    $wp_customize->add_control(
        'disable_parralax_post_hero',
        array(
            'type' => 'checkbox',
            'label' => __('Disable parralax effect.', 'kurayami'),
            'section' => 'general_section',
        )
    );
