<?php
/*  =============================================== */
//  # HEADER SECTION
/*  =============================================== */
$wp_customize->add_section( 'header_section', array(
    'priority' => 10,
    'capability' => 'edit_theme_options',
    'theme_supports' => '',
    'title' => __( 'Header', 'kurayami' ),
    'description' => '',
    'panel' => 'theme_settings',
) );

    /*  =============================================== */
    //  ## TOP BAR
    /*  =============================================== */
    $wp_customize->add_control(
        new kurayami_Customizer_Accordion(
            $wp_customize,
            'kurayami-header-top-bar-accordion',
            array(
                'section' => 'header_section',
                'label' => __( 'Top Bar', 'kurayami' ),
                'type' => 'accordion'
            )
        )
    );   

        $wp_customize->add_setting( 'display_search_in_top_bar', array(
            'default' => true,
            'transport' => 'refresh',
        ) );
        $wp_customize->add_control(
            'display_search_in_top_bar',
            array(
                'type' => 'checkbox',
                'label' => __('Display Search in Top Bar.', 'kurayami'),
                'section' => 'header_section',
            )
        );
    /*  =============================================== */
    //  ## HEADER
    /*  =============================================== */
    $wp_customize->add_control(
        new kurayami_Customizer_Accordion(
            $wp_customize,
            'kurayami-header-header-accordion',
            array(
                'section' => 'header_section',
                'label' => __( 'Header', 'kurayami' ),
                'type' => 'accordion'
            )
        )
    );  

        $wp_customize->add_setting(
            'header_height',
            array(
                'default' => '200px',
                'transport' => 'refresh',
                'sanitize_callback' => 'sanitize_text_field',
            )
        );
        $wp_customize->add_control(
            'header_height',
            array(
                'label' => __('Header height', 'kurayami'),
                'description' => __('In px, %, vh or vw.', 'kurayami'),
                'section' => 'header_section',
                'type' => 'text'
            )
        );

        $wp_customize->add_setting( 
            'header_image_quality', 
            array(
                'capability' => 'edit_theme_options',
                'default' => 'full',
            )
        );     
        $wp_customize->add_control( 
            'header_image_quality', 
            array(
                'type' => 'select',
                'section' => 'header_section',
                'label' => __( 'Header Image Size' ),
                'description' => __( 'Might affect page loading speed.' ),
                'choices' => array(
                    'full' => __( 'Full', 'kurayami' ),
                    'large' => __( 'Large', 'kurayami' ),
                    'medium' => __( 'Medium', 'kurayami' ),
                ),
            )
        );

        $wp_customize->add_setting( 'display_page_title_in_header', array(
            'default' => true,
            'transport' => 'refresh',
        ) );
        $wp_customize->add_control(
            'display_page_title_in_header',
            array(
                'type' => 'checkbox',
                'label' => __('Display Page Title in Header.', 'kurayami'),
                'section' => 'header_section',
            )
        );