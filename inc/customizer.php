<?php
if ( class_exists( 'WP_Customize_Control' ) && ! class_exists( 'kurayami_Customizer_Header' ) ) :
    class kurayami_Customizer_Title extends WP_Customize_Control {
             public $settings = 'blogname';
             public $description = '';

             public function render_content() {
             switch ( $this->type ) {
                 default:
                 case 'heading':
                     echo '<span class="customize-control-title">' . esc_html( $this->label ) . '</span>';
                 break;
             }
         }
    }
   class kurayami_Customizer_Accordion extends WP_Customize_Control {
          public $settings = 'blogname';
          public $description = '';

          public function render_content() {
          switch ( $this->type ) {
              default:
              case 'accordion':
                  echo '
                   <div class="accordion-title" style="width: 100%; background: white; padding: 0px 14px; box-sizing: border-box; border: 1px solid #ddd; border-left: 3px solid #0073aa; margin: 10px 0;">
                       <h3>'. esc_html( $this->label ) .'</h3>
                   </div>';
              break;
          }
      }
  }
endif;

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function kurayami_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';

	$wp_customize->remove_section('colors');
	$wp_customize->remove_section('header_image');
	$wp_customize->remove_section('background_image');

	$wp_customize->remove_control('blogdescription');
	$wp_customize->remove_control('display_header_text');

	$wp_customize->add_panel( 'theme_settings', array(
	    'priority' => 10,
	    'capability' => 'edit_theme_options',
	    'theme_supports' => '',
	    'title' => __( 'Theme Settings', 'kurayami' ),
	    'description' => __( 'All the settings for kurayami Theme', 'kurayami' ),
	) );

    require get_template_directory() . '/inc/customizer-sections/general-section.php';
    require get_template_directory() . '/inc/customizer-sections/header-section.php';
    require get_template_directory() . '/inc/customizer-sections/footer-section.php';
}
add_action( 'customize_register', 'kurayami_customize_register' );

