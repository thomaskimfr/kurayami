<?php get_header(); ?>

    <div class="main-wrap light">
        <div class="container large">
            
            <div class="articles-grid">

                <?php 
                if (have_posts()) : while (have_posts()) : the_post();
                    require get_template_directory() . '/template-parts/article-card.php';
                endwhile; endif; 
                ?>

            </div>

        </div>
    </div>

<?php get_footer(); ?>