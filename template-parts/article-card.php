<?php
$terms = get_the_terms($post->ID, 'category');
$nice_terms_markup = "";
foreach($terms as $term) {
    $nice_terms_markup .= "<a href='". get_term_link($term->term_id) ."'>" . $term->name . "</a>";
}
?>
<div class="article-card-container">
    <div class="article-card">
        <a href="<?php echo get_permalink(); ?>" class="featured-image" data-bg="<?php echo  get_the_post_thumbnail_url($post->ID,'medium'); ?>"></a>
        <div class="article-metadatas">
            <div class="categories">
                <?php echo $nice_terms_markup; ?>
            </div>
            <a href="<?php echo get_permalink(); ?>" class="post-title"><?php the_title(); ?></a>
        </div>
    </div>
</div>
