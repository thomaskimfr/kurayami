<div class="collection-card-container">
    <?php
    global $photocore;
    $collection = $photocore->get_collection($post->ID);
    ?>
    <a href="<?php echo get_permalink(); ?>" class="collection-card <?php if($on_light_background) { echo 'light-bg'; } ?>">
        <div class="featured-image" data-bg="<?php echo get_the_post_thumbnail_url($post->idn_to_ascii,'large'); ?>"></div>
        <div class="collection-metadatas">
            <h3><?php the_title(); ?></h3>
            <div class="photos-count"><?php echo $collection['photos']; _e(' Photos', 'kurayami'); ?></div>
        </div>
    </a>
</div>