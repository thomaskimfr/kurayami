<?php get_header(); ?>

    <div class="main-wrap light">
        <div class="container large">
            
            <h3><?php _e( 'Articles Matching Your Search', 'kurayami'); ?></h3>
            <div class="articles-grid">
                <?php 
                /* Start the Loop */
                $post_exists = false;
                $collection_exists = false;
                $attachment_exists = false;
                if (have_posts()) {
                    while (have_posts()) : the_post();

                        if(get_post_type() == "post") {
                            $post_exists = true;
                            require get_template_directory() . '/template-parts/article-card.php';
                        }

                        if(get_post_type() == "meow_collection") {
                            $collection_exists = true;
                        }

                    endwhile; 
                }
                ?>

            </div>

            <?php         
            if(!$post_exists) {
                _e("<h4 class='empty-message'>Oh no ! We couldn't find any articles matching your search !</h4>", 'kurayami');
            }
            ?>

            <h3><?php _e( 'Collections Matching Your Search', 'kurayami'); ?></h3>
            <?php if($collection_exists) { ?>
            <div class="collections-grid">
            <?php 
                if (have_posts()) : while (have_posts()) : the_post();

                    if(get_post_type() == "meow_collection") {
                        $on_light_background = true;
                        require get_template_directory() . '/template-parts/collection-card.php';
                    }

                endwhile; endif; 
                ?>               
            </div>
            <?php } ?>
            <?php         
            if(!$collection_exists) {
                _e("<h4 class='empty-message'>Oh no ! We couldn't find any articles matching your search !</h4>", 'kurayami');
            }
            ?>
        </div>
    </div>

<?php get_footer(); ?>