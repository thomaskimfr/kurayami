<?php
	get_header(); 
?>

<div class="main-wrap light">
    <div id="post-<?php the_ID(); ?>" <?php post_class('container entry-content'); ?>>
        <?php 
        if (have_posts()) : while (have_posts()) : the_post();
            the_content();
        endwhile; endif; 
        ?>
    </div>
</div>
<div class="post-navigation-container">
    <?php 
    $prev_post = get_previous_post();
    if(!empty( $prev_post )) {
    ?>
    <a href="<?php echo get_permalink($prev_post->ID); ?>" class="previous-post-container">
        <div class="background" data-bg="<?php echo get_the_post_thumbnail_url($prev_post->ID, 'full'); ?>"></div>
        <div class="top-label">
            <?php _e('Previous Post', 'kurayami'); ?>
        </div>
        <h3><?php echo $prev_post->post_title; ?></h3>
    </a>
    <?php
    }
    $next_post = get_next_post();
    if(!empty( $next_post )) {
    ?>
    <a href="<?php echo get_permalink($next_post->ID); ?>" class="next-post-container">
        <div class="background" data-bg="<?php echo get_the_post_thumbnail_url($next_post->ID, 'full'); ?>"></div>
        <div class="top-label">
            <?php _e('Next Post', 'kurayami'); ?>
        </div>
        <h3><?php echo $next_post->post_title; ?></h3>
    </a>
    <?php
    }
    ?>
</div>

<?php if ( comments_open() ) { ?>
<div class="container comments-section">
    <?php comments_template(); ?>
</div>
</div>
<?php } ?>

<?php get_footer(); ?>