    <div class="site-footer">
        <?php
            $footer_left_active = is_active_sidebar( 'footer-left' );
            $footer_middle_active = is_active_sidebar( 'footer-middle' );
            $footer_right_active = is_active_sidebar( 'footer-right' );
        ?>
        <div class="footer-widget-area container large">
                <?php if($footer_left_active) { ?>
                    <div class="widget-column">
                        <?php dynamic_sidebar( 'footer-left' ); ?>
                    </div>
                <?php } ?>
                <?php if($footer_middle_active) { ?>
                    <div class="widget-column">
                        <?php dynamic_sidebar( 'footer-middle' ); ?>
                    </div>
                <?php } ?>
                <?php if($footer_right_active) { ?>
                    <div class="widget-column">
                        <?php dynamic_sidebar( 'footer-right' ); ?>
                    </div>
                <?php } ?>
        </div>

        <div class="footer-credits">
            <?php if( get_theme_mod('footer_copyright_text', 'Kantan Theme by <a href="http://meowapps.com">MeowApps</a>') != "" ) { ?>
            <div class="site-info">
                <?php
                    echo get_theme_mod('footer_copyright_text', 'Kantan Theme by <a href="http://meowapps.com">MeowApps</a>');
                ?>
            </div><!-- .site-info -->
            <?php } ?>
            <?php if( get_theme_mod('display_sns_in_footer', true) ) { ?>
            <div class="social-networks">
                <?php if( get_theme_mod('instagram_url', '') !== '' ) { ?>
                <a href="<?php echo get_theme_mod('instagram_url'); ?>"><i class="fa fa-instagram"></i></a>
                <?php } ?>
                <?php if( get_theme_mod('facebook_url', '') !== '' ) { ?>
                <a href="<?php echo get_theme_mod('facebook_url'); ?>"><i class="fa fa-facebook-square"></i></a>
                <?php } ?>
                <?php if( get_theme_mod('twitter_url', '') !== '' ) { ?>
                <a href="<?php echo get_theme_mod('twitter_url'); ?>"><i class="fa fa-twitter-square"></i></a>
                <?php } ?>
                <?php if( get_theme_mod('cinqcentpx_url', '') !== '' ) { ?>
                <a href="<?php echo get_theme_mod('cinqcentpx_url'); ?>"><i class="fa fa-500px"></i></a>
                <?php } ?>
                <?php if( get_theme_mod('flickr_url', '') !== '' ) { ?>
                <a href="<?php echo get_theme_mod('flickr_url'); ?>"><i class="fa fa-flickr"></i></a>
                <?php } ?>
            </div>
            <?php } ?>
        </div>
    </div>
    
    <?php wp_footer(); ?> 
    </body>
</html>