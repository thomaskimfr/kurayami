<?php
function kurayami_scripts() {
    wp_enqueue_style( 'main-style', get_stylesheet_uri() );
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/css/fontawesome.min.css' );
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'vue', 'https://cdn.jsdelivr.net/npm/vue@2.5.16/dist/vue.js');
    wp_enqueue_script( 'masonry', 'https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js');
    wp_enqueue_script( 'main-js', get_template_directory_uri() . '/js/main.js', array('jquery'), null, true );
    if(is_page_template('page-templates/template-collections-page.php')) {
        wp_enqueue_script( 'collections-pages-js', get_template_directory_uri() . '/js/collections-page.js', array('jquery', 'vue', 'masonry'), null, true );
    }
}
add_action( 'wp_enqueue_scripts', 'kurayami_scripts' );

add_theme_support( 'post-thumbnails' );
add_theme_support( 'html5', array( 'comment-list', 'comment-form', 'search-form', 'gallery', 'caption' ) );

register_nav_menus( array(
    'menu-1' => esc_html__( 'Primary', 'kurayami' ),
) );

/**
 * Custom comment template
 */
function kurayami_comment_template($comment, $args, $depth) {
    $GLOBALS['comment'] = $comment; ?>
    <li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
        <div id="comment-<?php comment_ID(); ?>">
            <div class='comment-header'>
                <div class='comment-author-profil-picture'>
                    <?php echo get_avatar($comment,$size='48'); ?>
                </div>
                <div class="comment-meta">
                    <?php echo get_comment_author_link(); ?><br/>
                    <span class="posted-on"><?php echo get_comment_date(); ?></span>
                </div>
            </div>

            <div class='comment-content'>
                <?php if ($comment->comment_approved == '0') : ?>
                    <em><?php _e('Your comment is awaiting moderation.', 'nana') ?></em>
                    <br />
                <?php endif; ?>
                <?php comment_text() ?>
            </div>
            <div class='reply-link'>
                <?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
            </div>
        </div>
</li>
<?php
}

// Remove the logout link in comment form
add_filter( 'comment_form_logged_in', '__return_empty_string' );

// Register Widgets
function kurayami_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Footer • Left', 'kurayami' ),
		'id'            => 'footer-left',
		'description'   => esc_html__( 'Add widgets here.', 'kurayami' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer • Middle', 'kurayami' ),
		'id'            => 'footer-middle',
		'description'   => esc_html__( 'Add widgets here.', 'kurayami' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Footer • Right', 'kurayami' ),
		'id'            => 'footer-right',
		'description'   => esc_html__( 'Add widgets here.', 'kurayami' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
}
add_action( 'widgets_init', 'kurayami_widgets_init' );

require get_template_directory() . '/inc/customizer.php';