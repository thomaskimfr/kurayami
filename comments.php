<?php
/**
 * The template for displaying comments
 *
 * This is the template that displays the area of the page that contains both the current comments
 * and the comment form.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package kurayami
 */

/*
 * If the current post is protected by a password and
 * the visitor has not yet entered the password we will
 * return early without loading the comments.
 */
if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

		<?php
		$args = array(
			'fields' => apply_filters(
				'comment_form_default_fields', array(
					'author' =>'<input id="author" name="author" type="text" placeholder="Name" value="' . esc_attr( $commenter['comment_author'] ) . '" />',
					'email'  => '<input id="email" name="email" type="text" placeholder="Email" value="' . esc_attr(  $commenter['comment_author_email'] ) . '" />',
					'url'    => '<input id="url" name="url" type="text" placeholder="Website" value="' . esc_attr( $commenter['comment_author_url'] ) . '" /></div>'
				)
			),
			'comment_field' => '<p class="comment-form-comment">' .
				'<textarea id="comment" name="comment" placeholder="'.__('Write an answer ...', 'kurayami').'" cols="45" rows="7" aria-required="true"></textarea>' .
				'</p>',
			'comment_notes' => '',
			'comment_notes_after' => '<div class="user-infos"><button type="submit" class="submit-button"><span>'.__('Post', 'kurayami').'</span></button>',
			'title_reply' => ''
		);
		comment_form($args);
		?>

		<?php
		if ( have_comments() ) : ?>

		<hr/>

		<?php the_comments_navigation(); ?>

		<ol class="comment-list">
			<?php
				wp_list_comments( array(
					'style'      => 'ol',
					'short_ping' => true,
					'callback' => 'kurayami_comment_template'
				) );
			?>
		</ol><!-- .comment-list -->

		<?php the_comments_navigation();

	endif; // Check for have_comments().
	?>

</div><!-- #comments -->
