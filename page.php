<?php
	get_header(); 
?>

<div class="main-wrap light">
    <div id="page-<?php the_ID(); ?>" class="container entry-content">
    <?php 
    if (have_posts()) : while (have_posts()) : the_post();
        the_content();
    endwhile; endif; 
    ?>
    </div>
</div>

<?php get_footer(); ?>