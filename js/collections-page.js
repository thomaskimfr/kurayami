jQuery( document ).ready( function( $ ) {

    Vue.component('submenu-item', {
        data: function () {
            return {
                count: 0
            }
        },
        props: ['item', 'currentActiveFolder'],
        methods: {
            changeActiveFolder: function(folder_name) {
                $('body').trigger('changeActiveFolder', [folder_name]);
            },
            isActiveFolder: function(folder_name) {
                if(folder_name == this.currentActiveFolder) {
                    return "active";
                } else {
                    return "";
                }
            }
        },
        template: '	<div class="sub-sub-menu"> \
                        <li v-if="item.folders == 0" v-on:click="changeActiveFolder(item.name);" :class="isActiveFolder(item.name)"> \
                            {{item.name}} \
                        </li> \
                        <li v-if="item.folders != 0"> \
                            {{item.name}} <i class="ionicons ion-arrow-down-b"></i> \
                            <submenu-item v-for="(item,index) in item.children" :key="index" :item="item" :current-active-folder="currentActiveFolder"></submenu-item> \
                        </li> \
                    </div>'
    })

    var app = new Vue({
        el: '#app',
        data: {
            masonry: null,
            current_active_folder: 'all',
            ordering_by: 'Name',
            search_filter: '',
            collections: php_data.collections,
            folders: php_data.folders
        },
        computed: {
            filtered_collections: function() {
                var app = this;
                var folder_filtered_collections = []; // Init empty
                // Folder filtering
                if(app.current_active_folder == 'all') {
                    folder_filtered_collections = app.collections;
                } else {
                    app.collections.forEach(function(collection) {
                        if(collection.folder == app.current_active_folder) {
                            folder_filtered_collections.push(collection);
                        }
                    });
                }

                // Search filtering
                var search_filtered_collections = [];
                folder_filtered_collections.forEach(function(collection) {
                    if(collection.name.toUpperCase().indexOf(app.search_filter.toUpperCase()) > -1) {
                        search_filtered_collections.push(collection);
                    }
                });
                return search_filtered_collections;
            },
            ordered_collections: function() {
                if(this.ordering_by == 'Name') {
                    return this.filtered_collections.sort(function(col_a, col_b){
                        // Z goes last
                        if(col_a.name < col_b.name) return -1;
                        // A goes first
                        if(col_a.name > col_b.name) return 1;
                        return 0;
                    });
                }
                if(this.ordering_by == 'Publishing Date') {
                    return this.filtered_collections.sort(function(col_a, col_b){
                        // newest goes first
                        if(col_a.createdOn < col_b.createdOn) return 1;
                        // oldest goes last
                        if(col_a.createdOn > col_b.createdOn) return -1;
                        return 0;
                    });           
                }
                if(this.ordering_by == '# of Photos') {
                    return this.filtered_collections.sort(function(col_a, col_b){
                        // More photos goes first
                        if(col_a.photos < col_b.photos) return 1;
                        // Less photos goes last
                        if(col_a.photos > col_b.photos) return -1;
                        return 0;     
                    });          
                }
                return this.filtered_collections;
            }
        },
        methods: {
            test: function() {
                alert('test');

            },
            isActiveFolder: function(folder_name) {
                if(folder_name == this.current_active_folder) {
                    return 'active'
                } else {
                    return ''
                }
            },
            isParentFolder: function(folder) {
                if(folder.folders) {
                    return 'parent-folder'
                } else {
                    return ''
                }
            },
            changeOrderingBy: function(new_ordering_by) {
                this.ordering_by = new_ordering_by;
            }
        },
        mounted: function() {
            var app = this;

            $('.fullpage-loader').removeClass('visible');

            window.populateBackgrounds();

            this.masonry = $('.collections-grid').masonry({
                itemSelector: '.collection-card-container',
                transitionDuration: 0
            });

            $('body').on('changeActiveFolder', function(e, name) {
                app.current_active_folder = name;
            });

            // Main Wrap Top Bar: Search Field
            $('body').on('click', function(e) {
                if(!$(e.target).parents('.main-wrap-top-bar .search-filter').length > 0) {
                    if($('.main-wrap-top-bar .search-filter').hasClass('active')) {
                        $('.main-wrap-top-bar .search-filter').removeClass('active');
                    }
                }
            });

            $('.main-wrap-top-bar .search-filter i').on('click', function() {
                $('.main-wrap-top-bar .search-filter').addClass('active');
                $('.main-wrap-top-bar .search-filter .search-field').focus();
            });
        },
        updated: function() {
            window.populateBackgrounds();

            this.masonry.masonry('reloadItems');	
            this.masonry.masonry('layout');			
        }
    });
});