jQuery( document ).ready( function( $ ) {

    window.populateBackgrounds = function() {
        jQuery('*[data-bg]').each(function(){
            var bg_src = jQuery(this).attr('data-bg');
            jQuery(this).css('background-size', 'cover');
            jQuery(this).css('background-position', 'center');
            jQuery(this).css('background-image', 'url('+bg_src+')');
        });
    }

    setTimeout(function() {
        jQuery('.fullpage-loader').removeClass('visible');
    }, 1000);

    $('body').on('click', function(e) {
        if(!$(e.target).parents('.top-bar .site-search').length > 0) {
            if($('.top-bar .site-search').hasClass('active')) {
                $('.top-bar .site-search').removeClass('active');
            }
        }
    });

    $('.top-bar .site-search i').on('click', function() {
        $('.top-bar .site-search ').addClass('active');
        $('.top-bar .site-search .search-field').focus();
    });

    window.populateBackgrounds();

    $(window).on('scroll', function() {
        var scrollTop = $(this).scrollTop();
        $('.header .background').css('transform', 'translate3d(0,'+ scrollTop/2 +'px,0)')
    });

    if($('.articles-grid').length > -1) {
        $('.articles-grid').masonry({
            itemSelector: '.article-card-container',
            transitionDuration: 0
        });
    }

    if($('.collections-grid').length > -1) {
        $('.collections-grid').masonry({
            itemSelector: '.collection-card-container',
            transitionDuration: 0
        });
    }

    $('.small-screen-menu-trigger').on('click', function() {
        $('.small-screen-menu').addClass('visible');
    });

    $('.close-small-screen-menu').on('click', function() {
        $('.small-screen-menu').removeClass('visible');
    });
});