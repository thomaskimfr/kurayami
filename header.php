<!DOCTYPE html>
<html>
    <head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<?php wp_head();?>
    </head>
    
    <body>
		<div class="fullpage-loader visible">
			<div class="sk-cube-grid">
				<div class="sk-cube sk-cube1"></div>
				<div class="sk-cube sk-cube2"></div>
				<div class="sk-cube sk-cube3"></div>
				<div class="sk-cube sk-cube4"></div>
				<div class="sk-cube sk-cube5"></div>
				<div class="sk-cube sk-cube6"></div>
				<div class="sk-cube sk-cube7"></div>
				<div class="sk-cube sk-cube8"></div>
				<div class="sk-cube sk-cube9"></div>
			</div>
		</div>

		<div class="top-bar">
			<div class="container flex-row">
				<a class="site-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a>
				<div class="site-navigation">
					<li class="small-screen-menu-trigger">
						<i class="fa fa-bars"></i> <?php _e('MENU', 'kurayami'); ?>
					</li>
					<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-1',
						'menu_id'        => 'primary-menu',
					) );
					?>
				</div>
				<?php
				if(get_theme_mod('display_search_in_top_bar', true)) {
				?>
				<div class="site-search">
					<form method="get" action="<?php echo home_url( '/' ); ?>">
						<input type="text" name="s" class="search-field" placeholder="Search ..."> <i class="fa fa-search"></i>
					</form>
				</div>
				<?php
				}
				?>
			</div>
		</div>

		<div class="small-screen-menu">
			<div class="close-small-screen-menu">
				<i class="fa fa-times"></i>
			</div>
			<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
			?>	
		</div>

		<?php if(!(!is_front_page() && is_home())) { ?>
		<div class="header" style="height: <?php echo get_theme_mod('header_height', '200px'); ?>">
			<?php
			// Get the featured image
			if($post) {
				$featured_image = get_the_post_thumbnail_url($post->ID, get_theme_mod('header_image_quality', 'large'));
			} else {
				$featured_image = "";
			}
			?>
			<div class="background" data-bg="<?php echo $featured_image; ?>"></div>
			<div class="background-filter"></div>
			<div class="header-title">
				<div class="container large">
					<?php if(is_archive()) { ?>
						<h1><?php the_archive_title(); ?></h1>
					<?php } elseif(is_search()) { ?>
						<h1><?php _e('The search results for: ', 'kurayami'); the_search_query(); ?></h1>
					<?php } else { 
						if(get_theme_mod('display_page_title_in_header', true)) { ?>
							<h1><?php the_title(); ?></h1>
					<?php	}
					} ?>
				</div>
			</div>
		</div>
		<?php } ?>